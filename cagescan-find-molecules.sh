#! /bin/bash
# Copyright 2016 Martin C. Frith

export LC_ALL=C  # sane sorting

d=1
g=0.5
n=2
p=20

helpText="Usage: $0 [options] my_READ1.fq > my.molecules.txt

Options:
  -h, --help   show this help message and exit
  -d DISTANCE  maximum distance for linking sequences (default=$d)
  -g COST      distance per length-1 gap (default=$g)
  -n COUNT     minimum number of sequences in a cluster (default=$n)
  -p LENGTH    length of sequence prefix to add to UMI (default=$p)"

printHelp () {
    echo "$helpText"
    exit
}

while getopts :d:g:n:p: opt
do
    case $opt in
	d)  d=$OPTARG
	    ;;
	g)  g=$OPTARG
	    ;;
	n)  n=$OPTARG
	    ;;
	p)  p=$OPTARG
	    ;;
	?)  printHelp
	    ;;
    esac
done
shift $(($OPTIND - 1))

tmp=${TMPDIR:-/tmp}/$$
trap 'rm -f $tmp.*' EXIT

# Test for the presence of the "FP" field produced by TagDust 2.
set -e
head -n1 "$@" | grep -q 'FP:' || (echo "$0 error: FP field not found in sequence names" && exit 1)
set +e

# read FASTQ sequences, and write each: sequenceName, UMI, sequencePrefix
sed -e 's/^@ *//' -e 's/ .*FP:/ /' -e 's/;.*//' "$@" |
awk '
NR % 4 == 1 {name = $1; umi = $2}
NR % 4 == 2 {print name, umi, substr($1, 1, '"$p"')}
' |
sort > $tmp.data

# write UMI joined to sequencePrefix in FASTA format
awk '{print ">" $1 "\n" $2 $3}' $tmp.data > $tmp.fasta

set -o pipefail

# run SlideSort, and write each: clusterID, sequenceName
ssmst_v2 -u -d "$d" -g "$g" -i $tmp.fasta -Z -U 2> /dev/null |
sed -e 's/(...)//g' -e 's/,$//' -e 's/^singleton: /id:,/' -e 's/: /,/' |
tr ',' '\n' |
awk '/^id:/ {++id} !/^id:/ {print id, $1}' |
sort -u |

# get clusters with >= n items, and write each: sequenceName, clusterID
awk '
$1 != id {if (count >= '"$n"') print list; id = $1; list = ""; count = 0}
         {list = list $2 " " $1 "\n"; ++count}
END      {if (count >= '"$n"') print list}
' ORS= |
sort > $tmp.clusters

retVal=$?  # https://stackoverflow.com/questions/26675681/how-to-check-the-exit-status-using-an-if-statement-using-bash
if [ $retVal -ne 0 ]; then
    echo "Error in the pipeline using SlideSort." 1>&2
    echo "Just in case, check that no sequence is shorter than $p." 1>&2
    exit $retVal
fi

# write each: clusterID, consensusUMI.serialNumber
join $tmp.clusters $tmp.data |            # join on sequenceName
cut -d' ' -f2,3 |                         # get clusterID, UMI
sort |
uniq -c |                                 # count UMIs per clusterID
sort -k2,2 -k1,1nr -k3,3 |                # sort by clusterID, count, UMI
awk '$2 != id {print $3, $2; id = $2}' |  # top UMI per clusterID
sort |                                    # sort by UMI
awk '$1 != umi {n = 0; umi = $1} {print $2, $1 "." (++n)}' |
sort > $tmp.ids

# write each: sequenceName, consensusUMI.serialNumber
awk '{print $2, $1}' $tmp.clusters |  # get clusterID, sequenceName
sort |
join - $tmp.ids |
awk '{print $2 "\t" $3}' |
sort
