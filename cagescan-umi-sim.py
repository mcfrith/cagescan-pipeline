#! /usr/bin/env python
# Copyright 2017 Martin C. Frith

import optparse
import random
import signal

def cagescanUmiSim(opts, args):
    umiLen = opts.length
    seqLen = 30
    seqNum = int(args[0])
    random.seed(opts.seed)
    seq = "".join(random.choice("ACGT") for i in range(seqLen))
    for i in range(seqNum):
        umi = "".join(random.choice("ACGT") for i in range(umiLen))
        print "@seq%s:umi=%s FP:%s" % (i + 1, umi, umi)
        print seq
        print "+"
        print "I" * seqLen

if __name__ == "__main__":
    signal.signal(signal.SIGPIPE, signal.SIG_DFL)  # avoid silly error message
    usage = "%prog [options] num-of-sequences > out.fastq"
    description = "Simulate random UMIs (unique molecular identifiers)."
    op = optparse.OptionParser(usage=usage, description=description)
    op.add_option("--length", type="int", default=8, metavar="BP",
                  help="UMI length (default=%default)")
    op.add_option("--seed", type="int", default=12345,
                  help="seed for pseudo-randomness (default=%default)")
    opts, args = op.parse_args()
    if len(args) == 1:
        cagescanUmiSim(opts, args)
    else:
        op.print_help()
