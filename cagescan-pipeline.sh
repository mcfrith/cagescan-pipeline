#! /bin/sh
# Copyright 2016 Martin C. Frith

PATH=$PATH:$(dirname $0):.

d=1
g=0.5
n=2
p=20

b=20
m=0.001

s=1e6

helpText="Usage:
  lastdb -uNEAR -R01 -P0 my-db genome.fasta
  $0 [options] my-db tagdust-basename out-basename

Options:
  -h, --help   show this help message and exit

Options for find-molecules:
  -d DISTANCE  maximum distance for linking sequences (default=$d)
  -g COST      distance per length-1 gap (default=$g)
  -n COUNT     minimum number of sequences in a cluster (default=$n)
  -p LENGTH    length of sequence prefix to add to UMI (default=$p)

Options for build-transcripts:
  -b B         merge exons separated by <= B bases (default=$b)
  -m PROB      omit exons without mismap probability <= PROB (default=$m)

Options for to-bed:
  -s B         use separate lines for exons separated by > B bases (default=$s)"

printHelp () {
    echo "$helpText"
    exit
}

while getopts :d:g:n:p:b:m:s: opt
do
    case $opt in
	d)  d=$OPTARG
	    ;;
	g)  g=$OPTARG
	    ;;
	n)  n=$OPTARG
	    ;;
	p)  p=$OPTARG
	    ;;
	b)  b=$OPTARG
	    ;;
	m)  m=$OPTARG
	    ;;
	s)  s=$OPTARG
	    ;;
	?)  printHelp
	    ;;
    esac
done
shift $(($OPTIND - 1))

test $# -eq 3 || printHelp

read1="$2"_READ1.fq
read2="$2"_READ2.fq

cagescan-find-molecules.sh -d"$d" -g"$g" -n"$n" -p"$p" "$read1" > "$3".molecules.txt

cagescan-assemble.sh "$read1" "$read2" "$3".molecules.txt > "$3".assembled.fq

cagescan-map.sh "$1" "$3".assembled.fq > "$3".maf

cagescan-count-hits.sh "$3".maf | awk '{print $0 "\t'"$3"'"}' > "$3".hits.txt

cagescan-build-transcripts.py -b"$b" -m"$m" "$3".maf > "$3".transcripts.txt

cagescan-to-bed.py -s"$s" "$3".transcripts.txt |
sort -k1,1 -k2,2n -k3,3n -k4,4 -k6,6 > "$3".bed
