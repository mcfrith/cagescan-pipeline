#! /bin/sh
# Copyright 2016 Martin C. Frith

[ $# -eq 2 ] || {
    echo "Usage:"
    echo "  lastdb -uNEAR -R01 -P0 my-db genome.fasta"
    echo "  $0 my-db contigs.fq > out.maf"
    exit 2
}

threads=0  # xxx this should probably be a program option
m=100  # ?

# Append this info to each contig's name:
# 1. how many READ1s it contains
# 2. how many READ2s it contains
# 3. how many READ2 "tagments" it contains
# (4,5,...) READ1 start coordinates in the contig (zero-based)
awk '
NR % 4 == 1 {
  r1 = 0  # number of READ1s
  r2 = 0  # number of READ2s
  t2 = 0  # number of READ2 "tagments"
  b1 = ""  # READ1 start coordinates
  c1 = -1e9  # coordinate of previous READ1
  c2 = -1e9  # coordinate of previous READ2
  for (i = 2; i <= NF; ++i) {
    split($i, a, "[/@]")
    if (a[2] == "1") {
      ++r1
      if (a[3] > c1) {
        c1 = a[3]
        b1 = b1 "." c1
      }
    } else {
      ++r2
      if (a[3] > c2) {
        c2 = a[3]
        ++t2
      }
    }
  }
  $0 = $1 "." r1 "." r2 "." t2 b1
}
1
' "$2" |

lastal -Q1 -D10 -i4M -m$m -P$threads "$1" |

last-split -m1 -g "$1"
