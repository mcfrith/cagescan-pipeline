#! /usr/bin/env python
# Copyright 2016 Martin C. Frith

import optparse, re, signal, sys

def myOpen(fileName):  # faster than fileinput
    if fileName == '-': return sys.stdin
    else:               return open(fileName)

def exonsFromText(text):
    for i in re.split("[,;|]", text):
        chromosome, bounds = i.split(":")
        beg, end = bounds.split("-")
        beg = int(beg)
        end = int(end)
        assert beg != end
        if beg < end:
            yield chromosome, "+", beg, end
        else:
            yield chromosome, "-", end, beg

def isTransExons(x, y, maxDistance):
    xChromosome, xStrand, xBeg, xEnd = x
    yChromosome, yStrand, yBeg, yEnd = y
    if xChromosome != yChromosome or xStrand != yStrand:
        return True
    if xStrand == "+":
        return xEnd > yBeg or yBeg - xEnd > maxDistance
    else:
        return yEnd > xBeg or xBeg - yEnd > maxDistance

def exonGroups(exons, maxDistance):
    group = []
    for i in exons:
        if group and isTransExons(group[-1], i, maxDistance):
            yield group
            group = []
        group.append(i)
    if group:
        yield group

def isTss(read1offsets):
    if read1offsets == ".": return False
    a = read1offsets.split(";")
    b = [i.split(",") for i in a]
    c = sum(b, [])
    d = map(int, c)
    # Positive offsets tend to be preceded in the genome by the linker
    # sequence (e.g. a suffix of tataggg).  In one dataset, offset +4
    # was more common than +3.  This suggests that +4 offsets are
    # often not transcript starts.
    return max(d) < 4 and min(d) > -4  # ?

def doOneGroup(opts, otherFields, isTrans, groupNum, exons):
    moleculeId, x, x, tagments, x, x, read1offsets, x = otherFields
    head = exons[0]
    chromosome = head[0]
    strand = head[1]
    thickBeg, thickEnd = head[2:4]
    if strand == "-":
        exons.reverse()
    beg = exons[0][2]
    end = exons[-1][3]
    if groupNum > 0 or not isTss(read1offsets):
        thickBeg = beg
        thickEnd = beg
    if strand == "+":
        if isTrans: rgb = "127,255,127"  # pale green(?)
        else:       rgb = "0,127,0"      # dark green(?)
    else:
        if isTrans: rgb = "255,127,255"  # pale purple(?)
        else:       rgb = "127,0,127"    # dark purple(?)
    blockCount = len(exons)
    blockLens = ",".join(str(i[3] - i[2]) for i in exons)
    blockBegs = ",".join(str(i[2] - beg) for i in exons)
    out = (chromosome, beg, end, moleculeId, tagments, strand,
           thickBeg, thickEnd, rgb, blockCount, blockLens, blockBegs)
    print "\t".join(map(str, out))

def doOneLine(opts, fields):
    exonsText = fields[-1]
    otherFields = fields[:-1]
    exons = exonsFromText(exonsText)
    groups = list(exonGroups(exons, opts.max_sep))
    isTrans = (len(groups) > 1)
    for i, x in enumerate(groups):
        doOneGroup(opts, otherFields, isTrans, i, x)

def doOneFile(opts, lines):
    for line in lines:
        fields = line.split()
        if fields and fields[0][0] != "#":
            doOneLine(opts, fields)

def cagescanToBed(opts, args):
    if args:
        for i in args:
            doOneFile(opts, myOpen(i))
    else:
        doOneFile(opts, sys.stdin)

if __name__ == "__main__":
    signal.signal(signal.SIGPIPE, signal.SIG_DFL)  # avoid silly error message
    usage = "%prog [options] transcripts.txt > transcripts.bed"
    op = optparse.OptionParser(usage=usage)
    op.add_option("-s", "--max-sep", type="float", default=1e6, metavar="B",
                  help="use separate lines for exons separated by > B bases "
                  "(default=%default)")
    opts, args = op.parse_args()
    cagescanToBed(opts, args)
