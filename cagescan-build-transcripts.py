#! /usr/bin/env python
# Copyright 2016 Martin C. Frith

import itertools, operator, optparse, signal, sys

class MyError(Exception): pass

def uniqList(x):
    return [k for k, v in itertools.groupby(x)]

def myOpen(fileName):  # faster than fileinput
    if fileName == '-': return sys.stdin
    else:               return open(fileName)

def mafInput(lines):
    for line in lines:
        if line[0] == "a":
            rName = ""
            mismap = 0.0
            fields = line.split()
            for i in fields:
                # maybe get the score too?
                if i.startswith("mismap="):
                    mismap = float(i[7:])
        elif line[0] == "s":
            fields = line.split()
            if not rName:
                rName = fields[1]
                rBeg = int(fields[2])
                rEnd = rBeg + int(fields[3])
            else:
                qName = fields[1]
                qBeg = int(fields[2])
                qEnd = qBeg + int(fields[3])
                strand = fields[4]
                qLen = int(fields[5])
                if strand == "-":
                    qBeg, qEnd = qLen - qEnd, qLen - qBeg
                    rBeg, rEnd = -rEnd, -rBeg
                rNameAndStrand = rName + strand
                qf = qName.split(".")
                umi, serialNumber, contigId, read1s, read2s, tagments = qf[:6]
                read1starts = qf[6:]
                moleculeId = umi + "." + serialNumber
                yield [moleculeId, rNameAndStrand, rBeg, rEnd,
                       contigId, qBeg, qEnd, qLen, mismap,
                       read1s, read2s, tagments, read1starts]

def isGoodClump(alignments, maxMismap):
    return alignments and any(i[8] <= maxMismap for i in alignments)

def newClump(alignments):
    rNameAndStrand = alignments[0][1]
    return [alignments, rNameAndStrand]

def alignmentClumps(alignments, opts):
    clump = []
    old = ""
    for i in sorted(alignments):
        rNameAndStrand, rBeg, rEnd = i[1:4]
        if rNameAndStrand == old and rBeg <= maxEnd + opts.max_merge_distance:
            maxEnd = max(maxEnd, rEnd)
            clump.append(i)
        else:
            if isGoodClump(clump, opts.max_mismap_prob): yield newClump(clump)
            old = rNameAndStrand
            maxEnd = rEnd
            clump = [i]
    if isGoodClump(clump, opts.max_mismap_prob): yield newClump(clump)

def alignmentsFromClumps(clumps):
    for i in clumps:
        for j in i[0]:
            yield j

def appendLinksToAlignments(alignments):
    f = len(alignments) - 1
    for i, x in enumerate(alignments):
        contigId = x[4]

        prev = None
        if i > 0 and alignments[i-1][4] == contigId:
            prev = alignments[i-1]
        x.append(prev)

        next = None
        if i < f and alignments[i+1][4] == contigId:
            next = alignments[i+1]
        x.append(next)

def appendClumpsToAlignments(clumps):
    for i in clumps:
        for j in i[0]:
            j.append(i)

def isFirstAlignmentOfContig(a):
    return not a[13]

def isAlignmentOfRead1(a):
    return a[9] != "0"

def isCapAlignment(a):
    return isFirstAlignmentOfContig(a) and isAlignmentOfRead1(a)

def isCapClump(clump):
    return any(isCapAlignment(i) for i in clump[0])

def goodBegs(clump):
    for i in clump[0]:
        prev = i[13]
        if prev and prev[15] is not clump and prev[6] == i[5]:
            yield i[2]

def goodEnds(clump):
    for i in clump[0]:
        next = i[14]
        if next and next[15] is not clump and next[5] == i[6]:
            yield i[3]

def clumpBeg(clump):
    beg = min(i[2] for i in clump[0])
    maxCount = 0
    for k, v in itertools.groupby(sorted(goodBegs(clump))):
        count = len(list(v))
        if count > maxCount:
            beg = k
            maxCount = count
    return beg

def clumpEnd(clump):
    end = max(i[3] for i in clump[0])
    maxCount = 0
    for k, v in itertools.groupby(sorted(goodEnds(clump))):
        count = len(list(v))
        if count >= maxCount:
            end = k
            maxCount = count
    return end

def prevClumps(clump):
    for i in clump[0]:
        prev = i[13]
        if prev:
            prevClump = prev[15]
            if prevClump is not clump:
                yield prevClump

def nextClumps(clump):
    for i in clump[0]:
        next = i[14]
        if next:
            nextClump = next[15]
            if nextClump is not clump:
                yield nextClump

def appendEdgesToClump(clump):
    beg = clumpBeg(clump)
    end = clumpEnd(clump)
    if beg >= end:
        raise MyError("confusing exon")
    clump.append(beg)
    clump.append(end)

def clumpLen(clump):
    return clump[3] - clump[2]

def linkedClumpsLen(linkedClumps):
    return sum(map(clumpLen, linkedClumps))

def isFirstClump(clump):
    return not list(prevClumps(clump))

def isVisited(clump):
    return len(clump) > 2

def linkedClumps(clump):
    while True:
        if isVisited(clump):
            raise MyError("multi-acceptor")
        appendEdgesToClump(clump)
        yield clump
        n = uniqList(nextClumps(clump))
        if not n: break
        if len(n) > 1:
            raise MyError("multi-donor")
        clump = n[0]

def connectedComponents(clumps):
    capClumps = filter(isCapClump, clumps)
    if capClumps:
        if len(capClumps) > 1:
            raise MyError("multi TSS")
        yield list(linkedClumps(capClumps[0]))
    for i in clumps:
        if not isVisited(i) and isFirstClump(i):
            yield list(linkedClumps(i))
    if not all(isVisited(i) for i in clumps):
        raise MyError("circular splicing")

def read1offsetsText(alignment):
    qBeg = alignment[5]
    read1starts = alignment[12]
    return ",".join(str(int(i) - qBeg) for i in read1starts)

def clumpText(clump):
    rNameAndStrand, beg, end = clump[1:4]
    rName = rNameAndStrand[:-1]
    return rName + ":" + str(abs(beg)) + "-" + str(abs(end))

def linkedClumpSeparator(clump1, clump2):
    if len(list(goodEnds(clump1))) == 1 and len(list(goodBegs(clump2))) == 1:
        return ","
    else:
        return ";"

def textsAndSeparators(clumps):
    old = None
    for i in clumps:
        if old:
            yield linkedClumpSeparator(old, i)
        yield clumpText(i)
        old = i

def linkedClumpsText(clumps):
    return "".join(textsAndSeparators(clumps))

def doOneMolecule(opts, moleculeId, alignments):
    clumps = list(alignmentClumps(alignments, opts))
    if not clumps: return
    goodAlignments = list(alignmentsFromClumps(clumps))
    goodAlignments.sort(key=operator.itemgetter(4, 5))
    appendLinksToAlignments(goodAlignments)
    appendClumpsToAlignments(clumps)

    firstAlignments = filter(isFirstAlignmentOfContig, goodAlignments)
    read1s = sum(int(i[9]) for i in firstAlignments)
    read2s = sum(int(i[10]) for i in firstAlignments)
    tagments = sum(int(i[11]) for i in firstAlignments)

    basesInAlignedContigs = sum(i[7] for i in firstAlignments)
    alignedBases = sum(i[6] - i[5] for i in goodAlignments)
    unalignedBases = basesInAlignedContigs - alignedBases

    capAlignments = filter(isAlignmentOfRead1, firstAlignments)
    if capAlignments:
        unalignedBegs = ";".join(str(i[5]) for i in capAlignments)
        read1offsets = ";".join(map(read1offsetsText, capAlignments))
    else:
        unalignedBegs = "."
        read1offsets = "."

    try:
        components = list(connectedComponents(clumps))
    except MyError as e:
        out = (moleculeId, read1s, read2s, tagments,
               unalignedBases, unalignedBegs, read1offsets, e)
        print "#" + "\t".join(map(str, out))
        return

    length = sum(map(linkedClumpsLen, components))
    text = "|".join(map(linkedClumpsText, components))
    out = (moleculeId, read1s, read2s, tagments,
           unalignedBases, unalignedBegs, read1offsets, length, text)
    print "\t".join(map(str, out))

def doOneFile(opts, lines):
    for k, v in itertools.groupby(mafInput(lines), operator.itemgetter(0)):
        doOneMolecule(opts, k, v)

def cagescanBuildTranscript(opts, args):
    print "# column 1: molecule name"
    print "# column 2: number of READ1s in aligned contigs"
    print "# column 3: number of READ2s in aligned contigs"
    print "# column 4: number of READ2 tagments in aligned contigs"
    print "# column 5: number of unaligned bases in aligned contigs"
    print "# column 6: numbers of unaligned bases at starts of READ1 contigs"
    print "# column 7: READ1 start positions w.r.t. alignment start"
    print "# column 8: length of built transcript"
    print "# column 9: zero-based half-open coordinates of built transcript"
    if args:
        for i in args:
            doOneFile(opts, myOpen(i))
    else:
        doOneFile(opts, sys.stdin)

if __name__ == "__main__":
    signal.signal(signal.SIGPIPE, signal.SIG_DFL)  # avoid silly error message
    usage = "%prog [options] alignments.maf > out.txt"
    op = optparse.OptionParser(usage=usage)
    op.add_option("-b", "--max-merge-distance", type="int", default=20,
                  metavar="B", help=
                  "merge exons separated by <= B bases (default=%default)")
    op.add_option("-m", "--max-mismap-prob", type="float", default=0.001,
                  metavar="M", help=
                  "omit exons without mismap probability <= M "
                  "(default=%default)")
    opts, args = op.parse_args()
    cagescanBuildTranscript(opts, args)
