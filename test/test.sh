#! /bin/sh

try () {
    echo "# TEST" "$@"
    eval "$@"
    echo
}

d=$(dirname "$0")
cd "$d"

PATH=..:$PATH

{
    try trivissemble -h
    try trivissemble c33a_r1-1k.fq
    try trivissemble -t c33a_r1-1k.fq
    try trivissemble -l c33a_r1-1k.fq
    try trivissemble -m12 c33a_r1-1k.fq
} | diff -u test.txt -
