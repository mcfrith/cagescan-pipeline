# The CAGEscan pipeline

This pipeline reconstructs RNA molecules, from CAGEscan reads with
UMIs (unique molecular identifiers).  It performs these steps:

1.  Determine which reads come from the same molecule.

2.  For each molecule, assemble its reads.  This produces one or more
    contigs per molecule.

3.  Map the contigs to a reference genome, allowing for cis- and
    trans-splicing.

4.  Reconstruct the transcripts from the mappings.

Step 1 depends critically on the UMIs, in particular their length.  So
we include a simple simulator to test different UMI lengths.

## Requirements

### LAST

You need to have the LAST programs in your PATH.  You can get them
from [here](https://gitlab.com/mcfrith/last), and install them as
described there.

### SlideSort

You need to have `ssmst_v2` in your PATH.  You can get it like this:

    git clone https://github.com/iskana/SlideSort.git

If you do not have `git` installed, you can download and unpack:

    curl https://codeload.github.com/iskana/SlideSort/zip/master -o SlideSort.zip
    unzip SlideSort.zip
    mv SlideSort-master SlideSort
    
Then, build and install `ssmst_v2`:

    cd SlideSort
    /bin/sh mkall_v2.sh
    mkdir -p ~/bin
    cp mst_v2/ssmst_v2 ~/bin

If the compiler stops with an error, try to set `-std=c++98` as in
<https://github.com/iskana/SlideSort/issues/1>.

## Reference genome

Part of the pipeline requires a LAST index of a reference genome,
which you can create like this:

    lastdb -uNEAR -R01 -P0 my-db genome.fasta

This creates files with names starting in `my-db`.  (The `-P0` makes
it use as many threads as your computer claims it can handle
simultaneously.  You can replace it with e.g. `-P4` to use 4 threads.)

For (e.g.) a human genome, you need 15 GB of memory.

## Pipeline usage

The input to the pipeline is a pair of DNA read files produced by
`tagdust`, as shown here:

https://github.com/charles-plessy/tutorial/blob/master/Demultiplexing/Demultiplexing.md

For example, we can run the pipeline on a pair of files named
`DRR049557_BC_Caski_r1_READ1.fq` and `DRR049557_BC_Caski_r1_READ2.fq`
like this:

    cagescan-pipeline.sh my-db DRR049557_BC_Caski_r1 Caski_r1_out

The output of each pipeline step will be written to a file with name
starting in `Caski_r1_out`.

## Pipeline output

`cagescan-pipeline.sh` runs 6 commands, producing 6 output files:

| Step | Command                       | Output              |
| ---- | ----------------------------- | ------------------- |
| 1    | cagescan-find-molecules.sh    | out.molecules.txt   |
| 2    | cagescan-assemble.sh          | out.assembled.fq    |
| 3    | cagescan-map.sh               | out.maf             |
| 4    | cagescan-count-hits.sh        | out.hits.txt        |
| 5    | cagescan-build-transcripts.py | out.transcripts.txt |
| 6    | cagescan-to-bed.py            | out.bed             |

### Step 1: find-molecules

This step aims to group reads that come from the same molecule.  It
does so by single-linkage clustering of read-pairs with distance <= 1
in (UMI + first 20 bases of READ1), where a substitution has distance
1 and a length-1 gap has distance 0.5.  The output consists of many
lines like this:

    DRR049557.100001652	  TCCCGGGT.7

This means that read-pair `DRR049557.100001652` comes from molecule
`TCCCGGGT.7`, i.e. the 7th molecule with consensus UMI `TCCCGGGT`.

### Step 2: assemble

For each molecule, assemble its reads.  This produces contigs with
names like `AAAACAGC.4.2`, i.e. the 2nd contig for molecule
`AAAACAGC.4`.

### Step 3: map

This step maps the contigs to the reference genome.  The output is in
[MAF](http://genome.ucsc.edu/FAQ/FAQformat.html#format5) format.

### Step 4: count-hits

This counts the number of molecules mapped to each reference sequence
(i.e. each chromosome).

### Step 5: build-transcripts

This step reconstructs the transcripts from the mappings.  For each
molecule, it joins contigs whose mappings overlap or are very close.
The output has 9 columns, which are described in the header.

In the last column, there are three types of separators between
"exons":

 - Comma indicates a confidently-predicted intron. This means that one or more
   contigs span the intron, with no unaligned contig bases between the two
   flanking exons, and no disagreement between such contigs about the intron
   coordinates.

 - Semicolon indicates lower confidence. This means that one or more contigs
   span the "intron", but they have unaligned bases, or they disagree about the
   intron coordinates.

 - Pipe "|" indicates that no contigs span the gap.

### Step 6: to-bed

This step converts the transcripts to
[BED](https://genome.ucsc.edu/FAQ/FAQformat.html#format1) format.

Sometimes, one line in transcripts.txt describes a "trans splice", where
different parts of one transcript map to different chromosomes (or strands,
or non-co-linearly). It is impossible to represent this in one BED line. So such
cases are split into two (or more) BED lines.

Also, very long introns (by default > 1 million bases, but this can be changed
with the `--max-sep` option) get treated like a "trans splice" and split into
different BED lines for either side of the intron. The reason for this is that
huge introns cause trouble for visualization with e.g. Zenbu.

The color of each BED line indicates its strand, and whether or not
the transcript was split into multiple BED lines:

| Color                  | Strand | Multi-line ? |
|------------------------|--------|--------------|
| 0,127,0 (Green)        | Plus   | No           |
| 127,0,127 (Purple)     | Minus  | No           |
| 127,255,127 (Lime)     | Plus   | Yes          |
| 255,127,255 (Fuschia)  | Minus  | Yes          |

The 5'-most block of each BED line is made "thick" if and only if it
is deemed to represent a reliable transcription start site.
Therefore, a new BED file containing only entries which represent
transcription start sites can be made like this:

```bash
awk '$7 != $8' out.bed > out.clean.bed
```

You might have a high number of input BED files, use GNU parallel on all BED
files in a directory:

```bash
parallel "awk '\$7 != \$8' {} > {.}.clean.bed" ::: *.bed
```

Use above parallel command with the `--dry-run` option first to check what it does.

## Options

There are options to modify some of the pipeline's parameters.  To see
the options, run this command:

    cagescan-pipeline.sh --help

## UMI simulation

`cagescan-umi-sim.py` prints a number of *identical* fastq sequences,
each with a *random* UMI.  This simulates different RNA molecules from
the same transcription start site.  You can feed this into
`cagescan-find-molecules.sh` to see how many sequences are mistakenly
judged to come from the same molecule:

    cagescan-umi-sim.py --length=10 200 | cagescan-find-molecules.sh

This simulates 200 sequences with 10-base UMIs.

## Pipeline details

1.  Determine which reads come from the same molecule as each other.

    For each READ1, we concatenate the 8-base UMI to the first 20
    bases from the RNA, to form a ``true UMI''.  (The original UMI is
    not really unique: there are 65536 possible 8-base sequences, and
    probably more than 65536 RNA molecules per cell.  Even the true
    UMI is not foolproof: if one highly-active transcription start
    site produces many RNA molecules, their first 20 bases will be
    identical, and some of their 8-base UMIs are likely to coincide,
    according to the Birthday Paradox.)  To allow for sequencing
    errors, we link true UMIs with sequence distance $\le$ 1, where
    each substitution has distance 1 and each length-1 gap has
    distance 0.5, using SlideSort [@shimizu2011].  We then pull out
    connected components (i.e.  single-linkage clusters).  These
    indicate which read-pairs come from the same molecule as each
    other.

2.  For each molecule, assemble its reads.

    This is done after removing all linkers and 8-base UMIs, so we
    only have sequence from the RNA molecule.  The READ2s are
    reverse-complemented, so that all reads represent forward strands.

    a.  Trim Ns from each end of each read.

    b.  Merge each set of identical-sequence reads into one consensus
        fastq sequence.  The purpose of this step is to accelerate the
        subsequent steps.

        This read-merging procedure will be used later for
        non-identical sequences, so it is described here for
        not-necessarily-identical sequences.

        Suppose there are $n$ reads.  At each position, we have $n$
        bases $x_1, x_2, \ldots, x_n$ associated with $n$ fastq
        quality scores $q_1, q_2, \ldots, q_n$.  Each quality score
        represents an error probability: $e_i = 10^{-q_i/10}$.  The
        probability of the $i$th base being $y$ is:

        $p(i, y) =
        \begin{cases}
        1 - e_i & \text{if } y = x_i, \\
        e_i / 3 & \text{if } y \ne x_i.
        \end{cases}$

        By combining the $n$ observations, the probability of this
        base being $y$ is:

        $p(y) = \frac{m_y}{m_a + m_c + m_g + m_t}$

        where

        $m_y = \prod_{i=1}^n p(i, y)$

        The base with maximum $p(y)$ is chosen as the consensus, and
        its fastq quality score is obtained from $1 - p(y)$ in the
        standard way.
        

    c.  Overlap

        We used LAST (version >= 758) to find alignments between the
        reads:

            lastdb -Q1 -uNEAR -cR01 myDB reads.fastq
            lastal -Q1 -s1 -j1 -q30 -e90 -L15 -T1 myDB reads.fastq

        `lastdb` makes an index of the reads (called `myDB`), and
        `lastal` finds alignments.

        `-uNEAR` tunes it for finding near-exact matches.

        `-cR01` tells it to find [@frith2011a] and mask [@frith2011b]
        low-complexity sequence such as `atatatatat`.

        `-s1` requests same-strand alignments only.

        `-j1` specifies gapless alignment (to keep the subsequent
        assembly steps simple).

        `-q30` sets a stringent mismatch cost of 30.  (The match score
        defaults to 6.)

        `-Q1` tells it that the input is in fastq-sanger format. LAST
        adjusts the match and mismatch scores depending on the fastq
        qualities, as described in the supplement of [@frith2012].

        `-e90` requests alignments with score $\ge$ 90 (so at least 15
        matches).

        `-L15` sets a maximum seed length of 15.  (So it's guaranteed
        to find all length-15 exact matches.  It may also find inexact
        matches.)

        `-T1` specifies "overlap" alignment (i.e. the alignment must
        extend to the left until it hits the end of one or other
        sequence, and to the right until it hits the end of one or
        other sequence).

        In a postprocessing step, we discard self-alignments.

    d.  Layout

        We pull out connected components of reads that are linked by
        LAST alignments.  For each component, we determine the reads'
        layout in a trivial and naive way.  Each alignment indicates
        an offset between a pair of reads.  It is possible for these
        offsets to be inconsistent (e.g. reads X and Y are offset by
        1, reads Y and Z are offset by 1, but reads X and Z are offset
        by 7).  If there is any inconsistency, the algorithm gives up
        and outputs the reads separately.  Otherwise, the offsets
        determine the layout.

    e.  Consensus

        Layed-out reads are merged into one fastq consensus, by the
        read-merging procedure described above.

3.  Map contigs to a reference genome, allowing for cis- and
    trans-splicing.

    We mapped the assembled contigs to a reference genome with LAST:

        lastdb -uNEAR genomeDB genome.fasta

        lastal -Q1 -D10 -m100 genomeDB contigs.fastq |
        last-split -m1 -g genomeDB

    `-m100` makes it more slow and sensitive.  (The sensitivity boost
    makes little difference here, but since it is fast enough, we may
    as well do it.)

    `lastal` finds similar sequences (including unwanted paralogs),
    and then `last-split` filters these similarities to find a unique
    best alignment for each part of each contig [@frith2015].  The
    `-g` option puts it into spliced alignment mode (see the
    supplement).

    The output may have more than one alignment per contig, due to
    splicing.

4.  For each molecule, clump alignments with the same chromosome and
    strand, whose genomic coordinates overlap or are separated by up
    to 20 bases.  If all of a clump's alignments have mismap
    probability > 0.001, that clump is discarded.  The remaining
    clumps are used to define exons and introns.
