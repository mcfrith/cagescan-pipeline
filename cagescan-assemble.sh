#! /bin/sh
# Copyright 2016 Martin C. Frith

export LC_ALL=C  # sane sorting

PATH=$PATH:$(dirname $0):.  # try to ensure that trivissemble is in the PATH

[ $# -eq 3 ] || {
    echo "Usage: $0 READ1.fq READ2.fq molecules.txt > out.fq"
    exit 2
}

fastqRevcomp () {
    perl -ple '
if ($. % 4 == 2) {y/acgtrykmbdhvACGTRYKMBDHV/tgcayrmkvhdbTGCAYRMKVHDB/}
if ($. % 2 == 0) {$_ = reverse}
' "$@"
}

{
    awk 'NR % 4 == 1 {$0 = $1 " 1"} 1' "$1"

    awk 'NR % 4 == 1 {$0 = $1 " 2"} 1' "$2" | fastqRevcomp
} |
paste - - - - |
sed 's/^@ *//' |
sort |
join "$3" - |
sort -k2,2 |
awk '{print "@" $1 "/" $3 " mol=" $2 "\n" $4 "\n+\n" $6}' |
trivissemble
