#! /bin/sh
# Copyright 2017 Martin C. Frith

# Read pair-wise alignments in MAF format, and count the number of
# distinct molecules aligned to each reference sequence

# Assumes that the molecule ID is the prefix of the query sequence
# name, up to the second "."

export LC_ALL=C  # sane sorting

grep ^s "$@" |
cut -d' ' -f2 |
awk -F. 'NR % 2 == 0 {print r "\t" $1 "." $2} {r = $0}' |
sort -u |
cut -f1 |
uniq -c |
awk '{print $2 "\t" $1}'
